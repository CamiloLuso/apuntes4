# Crear material

1. `Crear material > Specular = 0`
2. `Color > Base Color > Image texture`

> `Ventana > UV Editor`

> Seleccionar con letra L

# UV Editor

Algoritmos para texturizado ir a:

1. `UV > Reset` (me da igual)
2. `UV > Cube|Cylinder|Sphere`
3. `UV > From View`
4. `UV > Mark Seam` (partir por las aristas seleccionadas)

Se puede editar como en modelado:

# Unity

> `Lighting > [x] Autogenerate`

1. Importar Asset
2. Importar Texturas

Si no asocia textura con objeto.

1. Crear color de Unity.
2. Asociar textura al color.

ó

1. Desbloquear color.
	1. Seleccionar objeto.
	2. Marterials > Materials > Extract Materials.

# Esferas

Buscar en Google: "Texture planet"
