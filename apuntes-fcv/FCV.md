# Unity

Clase MonoBehaviour → Unity lo evalua.

1. Awake().
2. Start() [VIP] Inicializar.
3. FixedUpdate().
4. Update() [VIP] Ejecución.

Get/Add/Remove Component son lentas y evita usarlas en Update().

## Clases más usadas

- Transform.
- Input.
- GameObject.
