# Animaciones

Se puede con posición, color, texturas...

# Lerp

`Lerp(P, Q, t)` donde P, Q son puntos y t una proporción tal que:

- (Interpolación) 0 ≤ t ≤ 1
- (Extrapolación) -∞ ≤ t ≤ ∞

## Problemas

- Picos
- Velocidades: constantes.
- Forma: linea vs. curva.

Todos estos problemas se solucionan con curvas Bézier.

# Unity

- Window > Animation > New
