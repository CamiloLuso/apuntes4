# Preferencias

- `File > External Data > Automatically pack into .blend`
- `Edit > Preferences > Keymap > Right, Search`

# Export

- `Export > FBX > Object types > Mesh...`

# Comandos

- g (Grab)
	- gg (Grab Local)
- r (Rotate)
- s (Scale)
- e (Extrude)
	- Alt + e
- Insert Faces
- Bevel

# ¡ALERTA! Blender → Unity

## Centro del objeto

> Unity no se puede cambiar.

1. `Edit > Seleccionar cara > Mesh > Snap > Cursor to Selected`
2. `Object > Set Origin > 3d Cursor`

## Sombraeado

- Cubo: `Object > Shade Flat`
- Esfera: `Object > Smooth Shade`
- Cilindro: `Object > Smooth Shade`
	- `Inspector > ∵ > Normals > Auto Smooth`

Si se pone tonto, `Modificador > Edge Split`

## Vértices duplicados

- `Mesh > Merge > By distance`

## Normales

- `Mesh > Normals > In/Out`

# Colores

- Prime color es el principal.
- `Edit > Seleccionar caras > Color > Assign` así podemos poner más materiales en Unity, donde se tunearán.
	- Al importar, Unity no los puede modificar → Extraerlos.
