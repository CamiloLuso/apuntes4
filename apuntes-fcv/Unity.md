# Config

- `New Project > Universal Render Pipeline (Shaders poco potentes)`
- `File >`
	- `Save (Scene)`
	- `Save Project`
	- `Build Settings > Windows > 64 > Build`
- `Edit >`
	- `Preferences > Editor`

> `Project > Assets` lo que yo debo acceder, el resto Unity.

# Posicionar cámara

1. Seleccionar cámara.
2. `Game Object > Align with view`

# Iluminación

`Window > Rendering > Lightning Settings > [x] Auto Generate`

# Consejos

- Empty object → hacer jerarquías.
- Empezar por los script de control.
	- El control no debe heredar de `MonoBehaviour` y debe ser `static`
- Referenciar con variable, nombre, tag del inspector

# Input

- `Input::`
	- `GetKey()` ¿pulsada?
	- `GetKeydown()` ¿acada de ser pulsada?
	- `GetKeyup()` ¿acada de ser soltada?
- `KeyCode`

## Ejes

`Edit > Project Settings > Input Manager > Axis > Horz ; Vect > Gravity ; Dead`

## Control

- `Object > Add Component > Control 2D (Script)`

```C#
[System.Serializable]
class MyInput
	// Nombre ejes y teclas
	String h = "Horizontal";
	KeyCode sp = KeyCode.Space;

	// Variables almacenan Input
	float hor;

	void GetInput() {
		hor = Input.GetAxis(sp);
		jump = Input.GetKey(...);
		shoot = Input.GetKeyDown(...);
	}
```

## Movimiento

1. ¿Qué eje? → Vector3.right
2. ¿Qué velocidad? → var v
3. Input horizontal → var hor
- `Transform.Translate(hor*v*Vevctor3.right, Space.World)`

## Código

1. Definir variables: visibles vs. no visibles.
2. Start: inicializar variables; arreglar normas de vectores a 1.
3. Update:
	1. Actualizar Input.
	2. Calcular dirección.
	3. Girar.
	4. Mover.

# Luz

- `Window > Rendering > Lightning Settings`

# Materiales

- `Materials > Crear`
- `Shader > Unlit # Sin sombras`
	- Color
	- Texture
- `Shader > Universal Render Pipeline`
	- Lit
	- Unlit
- `Shader > Standard`
- `Surface Options > Surface Type`
- `Surface Input >`
	- `Base map - Img + Color`
	- `Metalic`
	- `Normal # Relieve`
	- `Oclussion`

# Start()

```
Start()
	x.normalized()
	y.normalized()
```

# Update()

```
Update()
	miInput.GetInput()
	GetDirection()
	GetOrientation()
	Move()
	Pause() {
		if (letra.pausar)
			Control.dt = 0.0
		else
			Control.dt = 1.0
	}
```

# Pausa

Recuerda que Control es estático.

```
Control.dt = 0.0 // Pause
Control.dt = 1.0 // Resume
```
