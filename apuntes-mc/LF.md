# Lenguajes formales

## Ontología

- **Alfabeto**
    - es-un conjunto de *letras* finito no vacío.
- **Cadena**
    - es-una secuencia de *letras.*
- **Letra**
    - es-un símbolo.
- **Lenguaje**
    - es-un conjunto de *cadenas.*

**Nota:** un símbolo es un componente abstracto indivisible.

## Taxonomía

- **Tipo 0:** lenguaje recursivamente enumerable, lenguaje semidecidible, máquina de Turing que acepta o rechaza o entra en bucle infinito.
- **Tipo 1:** lenguaje dependiente de contexto, lenguaje recursivo, lenguaje decidible, autómata linealmente acotado, máquina de Turing (ALA, MT).
- **Tipo 2:** lenguaje independiente del contexto, autómata de pila (AP).
- **Tipo 3:** lenguaje regular, autómata finito (AF).

Los lenguajes se pueden ordenar según el dominio de cadenas que reconocen.

- **General:** LR < LIC < LDC < LRE.
- **Efiente:** LR > LIC > LDC > LRE.

**Conclusión**

- Los lenguajes generales (menos restrictivos) reconocen un mayor dominio pero son menos eficientes.
- Los lenguaje específicos (más restrictivos) reconocen un menor dominio pero son más eficientes.

**Analogía**

- **General:** ROM < ASIC < FPGA < CPU.
- **Efiente:** ROM > ASIC > FPGA > CPU.

Las CPU son más generales pero son menos eficientes que, por ejemplo, aceleradores de red, sonido, vídeo (GPU).

# Conceptos

## Alfabeto

- $`V_{dec} = \{ 0,1,2,3,4,5,6,7,8,9 \}`$
- $`V_{abc} = \{ a,b, ..., z \}`$
- $`V_{bin} = \{ 0,1 \}`$

## Cadena

- $`λ`$ cadena vacía tal que $`|λ| = 0`$
    - $`λ \not \in V`$ porque es una cadena, no un símbolo.

**Ejemplo**

- $`x = 1, x \in V_{dec}^*`$
- $`x = λ, x \in V_{dec}^*`$
- $`x = 128, x \in V_{dec}^*`$

## Letra

- $`z \in V_{abc}`$
- $`128 \not \in V_{dec}`$
- $`aceptar \in V_{eventos}`$

## Lenguaje

> $`L`$ es el conjunto de *cadenas* reconocidas según una definición (extensión, intensión). Se cumple $`L \subseteq V^*`$

```math
V^* = \bigcup_{n = 0}^{∞} V^n
```

```math
V^+ = \bigcup_{n = 1}^{∞} V^n
```

- $`V^*`$ lenguaje universal es un conjunto infinito no vacío con todas las *cadenas* con todas las combinaciones de *letras* del *alfabeto.*
- $`V^+`$ lenguaje universal positivo tal que $`V^* = \{λ\} \cup V^+`$
- $`\varnothing`$: lenguaje vacío.
    - $`\varnothing ≠ \{ λ \} ≠ λ`$

**Ejemplo**

- $`L_{2bits} = \{ 00,01,10,11 \}`$
- $`L_{primos} = \{ x \in V_{dec}^* \ | \ P(x)\}`$

**Nota:** la definición por extensión es para lenguajes finitos, mientras que la definición por intensión es para lenguajes finitos o infinitos.

# Funciones

## Cadenas

**Prioridad**

1. Potencia, Reflexión.
2. Concatenación.
3. Longitud.

### Longitud

```
| | :: V* -> N
|x| = length x
```

$`|x| = 0 ⇔ x = λ`$

### Concatenación

```
+ :: V* -> V* -> V*
x + y = xy
```

### Potencia

```
^ :: V* -> N -> V*
x ^ 0 = λ
x ^ n = x + (x ^ (n-1)) = (x ^ (n-1)) + x
```

### Reflexión

```
R :: V* -> V*
R λ = λ
R (a + x) = (R x) + a
```

## Lenguaje

### Conjuntos

- **Unión**
    - $`L_1 \cup L_2 = \{ x\ |\ x \in L_1 \vee x \in L_2 \}`$
- **Intersección**
    - $`L_1 \cap L_2 = \{ x\ |\ x \in L_1 \wedge x \in L_2 \}`$
- **Diferencia**
    - $`L_1 - L_2 = \{ x\ |\ x \in L_1 \wedge x \notin L_2 \}`$
- **Complemento**
    - $`\overline{L} = V^{*} - L`$

### Propiedades

- **Asociativa**
	- $`L_1 \cap (L_2 \cap L_3) = (L_1 \cap L_2) \cap L_3`$
	- $`L_1 \cup (L_2 \cup L_3) = (L_1 \cup L_2) \cup L_3`$
	- $`L_1 \circ (L_2 \circ L_3) = (L_1 \circ L_2) \circ L_3`$
- **Conmutativa**
	- $`L_1 \cap L_2 = L_2 \cap L_1`$
	- $`L_1 \cup L_2 = L_2 \cup L_1`$
- **Distributiva**
	- $`L_1 \cap (L_2 \cup L_3) = (L_1 \cap L_2) \cup (L_1 \cap L_3)`$
	- $`L_1 \cup (L_2 \cap L_3) = (L_1 \cup L_2) \cap (L_1 \cup L_3)`$
	- $`L_1 \circ (L_2 \cup L_3) = (L_1 \circ L_2) \cup (L_1 \circ L_3)`$
	- $`(L_2 \cup L_3) \circ L_1 = (L_2 \circ L_1) \cup (L_3 \circ L_1)`$
- **Ley D'Morgan**
	- $`\overline{L_1 \cap L_2} = \overline{L_1} \cup \overline{L_2}`$
	- $`\overline{L_1 \cup L_2} = \overline{L_1} \cap \overline{L_2}`$
- **Complemento**
	- $`\overline{L} \cap L = \varnothing`$
	- $`\overline{L} \cup L = V^{*}`$
	- $`\overline{\overline{L}} = L`$
- **Lenguaje vacío**
	- $`L \cap \varnothing = \varnothing`$
	- $`L \cup \varnothing = L`$
- **Lenguaje universal**
	- $`L \cap V^{*} = L`$
	- $`L \cup V^{*} = V^{*}`$
- **Elemento nulo**
    - $`L \circ \varnothing = \varnothing`$
- **Elemento identidad**
    - $`L \circ \{ \varnothing \} = \{ \varnothing \} \circ L`$


### Concatenación

```
+ :: {V*} -> {V*} -> {V*}
L1 + L2 = {x + y | x <- L1, y <- L2}
```

**Ejemplo**

- $`L_1 = \{ab, c, λ\} + L_2 = \{cc, a\}`$
    - $`ab + cc`$
    - $`ab + a`$
    - $`c + cc`$
    - $`c + a`$
    - $`cc`$
    - $`a`$

### Potencia

```
^ :: {V*} -> N -> {V*}
L ^ 0 = {λ}
L ^ n = L + (L ^ (n-1))
```

**Ejemplo**

- $`L = \{ab, c\}`$
    - $`L^0 = \{λ\}`$
    - $`L^1 = \{ab, c\} + \{λ\}`$
    - $`L^2 = \{ab, c\} + \{ab, c\}`$
        - $`ab + ab`$
        - $`ab + c`$
        - $`c + ab`$
        - $`c + c`$
    - $`L^3 = \{ab, c\} + \{abab, abc, cab, cc\}`$
        - $`ab + abab`$
        - $`ab + abc`$
        - $`ab + cab`$
        - $`ab + cc`$
        - $`c + abab`$
        - $`c + abc`$
        - $`c + cab`$
        - $`c + cc`$

### Reflexión

```
R :: {V*} -> {V*}
R L = {R x | x <- L}
```

**Ejemplo**

- $`L = \{ab, c\}`$
    - $`ba`$
    - $`c`$

### Clausura de Kleene

```math
L^* = \bigcup_{n = 0}^{∞} L^n
```

```math
L^+ = \bigcup_{n = 1}^{∞} L^n
```

- $`L^*`$ clausura de Kleene universal es un conjunto infinito no vacío con todos los lenguajes.
    - $`L_0 = \{λ\}`$
- $`L^+`$ clausura de Kleene positiva tal que $`L^* = \{λ\} \cup L^+`$
    - Aunque no necesariamente $`λ \in L^+`$ aunque es posible $`λ \in L ⇒ λ \in L^+`$
