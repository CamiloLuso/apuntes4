# Decidibilidad

## Teorema I

$`Acc^{DFA} = \{ \langle A, w\rangle \}`$ lenguaje que evalúa si A acepta w.

> $`Acc^{DFA} \in DEC`$

Porque codificamos A (se puede) y codificamos w (trivial) y simulamos la evaluación.

## Teorema II

> $`Acc^{NFA}, Acc^{PDA} \in DEC`$

Porque tanmbién podemos codificar y simular los autómatas.

## Teorema III

$`Empty^{DFA} = \{ \langle A \rangle | L(A) = \varnothing\}`$ lenguaje que evalúa si A acepta lenguaje vacío.

> $`Empty^{DFA} \in DEC`$

Podríamos simular A pero es imposible pues no hay garantías de que termine (pues es necesario probar infinitas cadenas). Usar este algoritmo:

1. Marcar $`q_0`$
2. Infectar/Marcar todos los estados accesibles recursivamente.
3. ¿Estado final marcado?
    - Sí, rechaza.
    - No, acepta.

## Teorema IV

> $`Empty^{NFA} \in DEC`$

Podríamos simular pero no hay garantías → Algoritmo.

## Teorema V

> $`Empty^{PDA} \in DEC`$

Podríamos simular pero no hay garantías → Algoritmo.

## Pregunta

> $`Acc^{MT} = \{ \langle M, w \rangle \} \in^? DEC`$ ¿es decidible simular una MT?

> $`DEC \subseteq^? RE`$

> $`RE =^? U = 2^{Σ^*}`$

# Indecidibilidad

## Numeración de Gödel

> ¿El conjunto de todas las cadenas que describen MT es enumerable?

### Teorema VI

> El conjunto de todas las cadenas que describen MT es enumerable

1. Codificar cada elemento del lenguaje.
2. Codificar cada instrucción.
3. Codificar cada codificación de instrucción.

Junto con el teorema fundamental de la aritmética demostramos ya que multiplicación de primos es unívoca. Usar la función $`Π(n)`$ que devuelve el **n-ésimo número primo.** Hacemos:

```
Π(1)^c · Π(2)^c · ...
```

**Codificar lenguaje**

- 0 → 1
- S → 2
- V1 → 3
- V2 → 4
- V3 → 5
- := → 6

```
V1 := 0
V1 := S(V1)
```

**Codificar instrucciones**

1. `V1 := 0` es $`Π(1)^3 Π(2)^6 Π(3)^1 = 2^3 3^6 5^1 = 29160`$
2. `V1 := S(0)` es $`Π(1)^3 Π(2)^6 Π(3)^2 Π(4)^3 = 2^3 3^6 5^2 7^3 = 50009400`$

**Codificar codificación de instrucciones**

1. $`Π(1)^{29160} Π(2)^{50009400} = 2^{26160} 3^{50009400}`$

**Conclusión**

Todo programa (máquina MT) corresponde a un número natural. Por tanto, el conjunto de todas las cadenas que corresponden a MT es enumerable.

## Paradoja de Russell

No existe biyección entre el conjunto $`A`$ y su $`2^A`$. Suponer que $`f:A→2^A`$ es una biyección.

1. Suponer $`B = \{ x \in A | x \not \in f(x) \}`$.
2. Sabemos $`f(x) \subseteq A`$ (porque la imagen será una parte o todo A!)
3. Sabemos $`B \subseteq A`$ (porque B es A restringido con una condición)
4. $`\square \exists y \in A : f(y) = B`$ **pero**
5. $`y \in B → y \not \in f(y) → y \not \in B`$
6. $`y \not \in B → y \not \in f(y) → y \in B`$
7. Contradicción, no existe $`f`$ biyectiva.

![](img/russell.jpg)

Conjunto que contiene conjuntos que no se contienen a sí mismos.

## Teorema VII

> $`\exists L : L \not \in RE`$

- $`Σ`$ es enumerable.
- $`Σ^*`$ es enumerable.
- $`2^{Σ^*}`$ no es enumerable por la paradoja de Russell.
- MT son enumerables pero $`2^{Σ^*}`$ no es enumerable.
- Por tanto, existen lenguajes fuera de RE.

## Máquina Universal de Turing

MT que simula <MT, w> (mediante la numeración de Gödel).

## Teorema VIII

> $`DEC \subset RE`$

$`K = Acc^{MT} = \{ \langle M, w\rangle \}`$ lenguaje que evalúa si M acepta w.

- Sabemos $`K \in RE`$ (porque se puede codificar MT y simularla, acepta o rechaza o no termina) pero
- $`K \not \in DEC`$ porque...

![](img/kmtu.png)

La MTU reconoce el lenguaje K.

![](img/hdec.png)

Suponer que existe una MT decidible llamada H que recibe <M, w>.

![](img/ddec.png)

Crear MT llamada D que niega la H.

![](img/d1dec.png)

Crear MT llamada D' que recibe «M».

![](img/d1d1.png)

Aplicar «D'» a D'. Esto dice que:

- Acepta cuando D' rechaza «D'».
- Rechaza cuando D' acepta «D'».

Contradicción, H no existe → K no es decidible → K no es decidible.

**Conclusión**

El poder expresivo del lenguaje es suficiente como para poder hablar de sí mismo. Es como "esta frase es falsa" o los teoreas de incompletitud de Gödel.

## Teorema IX

> $`L, \overline{L} \in RE → L, \overline{L} \in DEC`$

- Sabemos $`L \in RE → \exists M `$ tal que MT acepta y termina.
- Sabemos $`\overline{L} \in RE → \exists M `$ tal que MT acepta y termina.
- Puesto que ambas MT terminan ambos lenguajes son **decidibles.**

# co-RE

Algunos $`L \not \in RE`$ pero sí $`L \in co-RE`$. Por ejemplo, $`\overline{K} \not \in RE`$ pero $`\overline{K} \in co-RE`$. En **co-RE no decimos nada del lenguaje,** decimos que **su complementario está RE.**

![](img/co-RE.jpg)

# Teorema X

> $`L \in DEC → L^* \in DEC`$

# Teorema XI

> $`L_1, L_2 \in DEC → L_1 ∩ L_2 \in DEC`$

# Teorema XII

> REG, CF, DEC, RE no están cerrados bajo la operación de subconjunto.

- Suponer $`L \in REG`$, por tanto, $`L`$ es enumerable.
- $`2^L`$ no es enumerable.
- Por tanto, existe un subconjunto de $`L`$ tal que $`L \not\in RE`$
- Lo mismo para CF, DEC, REG.

# Teorema XIII

> $`L_1, L_2 \in RE → L_1 ∪ L_2 \in RE`$

# Teorema XIV

> $`L_1, L_2 \in RE → L_1L_2 \in RE`$

# Teorema XV

> RE no está cerrado bajo complemento.

# Clausura

![](img/dec.jpg)

![](img/re.jpg)

# Resumen

![](img/resumen_re.jpg)
