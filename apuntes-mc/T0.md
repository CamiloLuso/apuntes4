# Computación

> **Computación = Computabilidad + Complejidad + Autómata**

- **Computabilidad:** ¿Qué es teóricamente computable? → {Sí, No}
    - **Complejidad:** ¿Qué es prácticamente computable? → {<, =, >}
- **Autómata:** estructura para definir modelos computacionales.

![](img/cc.png)

# Tesis Church-Turing

> Todo **algoritmo computable** es **computable por una Máquina de Turing.**

No es un teorema, es una tesis/evidencia ya que es indemostrable. Es una tesis porque se basa en una hipótesis (premisa informal) y no en un axioma (premisa formal). Se define la noción de algoritmo (informal) como una Máquina de Turing (formal). Si tuviésemos una definición formal de algoritmo sería un teorema.

La Máquina de Turing es el modelo más expresivo. Todos modelos suficientemente expresivos son equivalentes. Por tanto:

- **Máquina de Turing = Cálculo Lambda = Funciones Recursivas de Gödel**

# Teoremas de incompletitud de Gödel

## Teorema I

> No existe teoría formal tal que es capaz de expresar los números naturales y la aritmética con suficiente expresividad y, además, ser consistente y completa.

## Teorema II

> Si la teoría es consistente, entonces no es posible demostrar que es consistente mediante esa teoría.

El teorema es un caso particular del I. Dice que el teorema que demuestra que una teoría es consistente se encuentra fuera del alcance de esa teoría.

![](img/teoria.png)

# Conjuntos

## Producto Cartesiano

> Conjunto de todas las combinaciones.

```math
A × B = \{(a, b) | a \in A, b \in B\}, ∀a,b
```

## Conjunto Potencia

> Conjunto de todas las partes.

- $`A = \{1,2,3\}`$
    - $`P(A) = \{\varnothing, \{1\}, \{2\}, \{3\}, \{1,2\}, \{1,3\}, \{2,3\}, \{1,2,3\} \}`$
    - $`|P(A)| = 2^{|A|}`$

![](img/pc.png)

## Relación

> Subconjunto del producto cartesiano.

```math
R \subseteq A × B
```

![](img/r.png)

## Función

> Tipo de relación determinista.

```math
f \subset A × B \\
f : A → B
```

- $`A`$ dominio.
- $`B`$ codominio.

![](img/f.png)

### Propiedades

- **Total:** todo elemento en dominio tiene codominio.
- **Parcial:** no total.

![](img\fp.jpg)

## Teorema de Cantor

> Los infinitos son ordenables.

**Tipos**

- Numerable $`\mathbb{Z}`$.
- No numerable $`\mathbb{R}`$.

# Lenguajes

- **Alfabeto:** $`Σ`$
- **Cadena:** $`w`$
- **Cadena vacía:** $`ε`$
- **Cadena invertida:** $`w^{-1} = w^R`$

Nos interesan los lenguajes finitos o infinitos con cadenas finitas.

# Problema computacional

> Conjunto de cadenas de cardinalidad finita que tienen asociadas cadenas de cardinalidad finita.

## Problema decisión

> Pregunta con respuesta $`\mathbb{B}`$ = {Sí, No}.

El subconjunto de cadenas para las que la respuesta es sí es un **lenguaje formal.**

**Ejemplo:** números primos.

- $`P = \{2,3,5,7,11,13...\}`$
    - $`2 \in P = T`$
    - $`3 \in P = T`$
    - $`4 \in P = F`$

## Problema funcional

> Pregunta con respuesta $`\mathbb{N, Z, R ...}`$

Un problema funcional se puede transformar a uno decisional. Sus propiedades **computacionales** se mantienen pero **no necesariamente sus propiedades complejidad.**

- **Funcional**
    - $`L(\triangle) = 3`$
    - $`L(\square) = 4`$
- **Decisión**
    - $`(\triangle, 3) \in L = T`$
    - $`(\square, 4) \in L = T`$
    - $`(\square, 5) \in L = F`$

# Cosos

## Computabilidad

Aunque haya problemas que no tengan un **algoritmo genérico,** eso no significa que no exista un **algoritmo menos genérico** que sí resuelva el problema para menos entradas.

**Ejemplo:** los lenguajes libres de contexto no tienen un algoritmo genérico para determinar la ambiguedad de un lenguaje. Sin embargo, existen métodos que sí la detectan pero para menos entradas.

**Ejemplo II:** L1 no es decidible pero las cláusulas de Horn en L1 sí.

## Complejidad

Ante problemas difíciles podemos:

- Transformar en otro más fácil.
- Obtener una solución subóptima admisible.
- Aceptar el algoritmo si $`O(e^n)`$ pero $`Ω(n^c)`$.
- Aceptar el algoritmo si es muy probable el mejor caso.

