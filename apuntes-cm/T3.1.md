# Teoría de la información

> La **teoría de la información** _cuantifica_ la información existente en los datos.

Modelar algo cualitativo (información) como algo cuantitativo (información relevante). La función $`\log()`$ es la que cuantifica la información, ya que cumple las 4 condiciones necesarias.

Se basa en observar la cantidad de elementos sorprendentes de un mensaje.

# Modelo

- **Alfabeto:** conjunto n-ario formado por letras.
	- **Letra:** símbolo.
- **Palabra:** secuencia de letras.
	- **Longitud:** nº de letras.
	- A\*: conjunto de todas las palabras de un alfabeto A.
- **Mensaje:** letra o palabra o concatenación de ambas.
- **Código:** $`C(a) = p : A → A^{\ast}`$ donde:
	- **a:** es una letra.
	- **p:** es una **palabra clave** $`p \in A^{\ast}`$
	- **Extensión:** de orden n es C aplicado, no a letras sino a palabras de longitud n. El resultado sería la concatenación de cada resultado.
	- **Longitud:** nº de letras.
	- **Longitud media:** longitudes promedio ponderadas por probabilidades. $`\bar{n} = \sum_{i=1}^{n} p_i·n_i`$

## Código

![Clasificación de códigos](img/clase_codigos.png)

### No singular

> $`C`$ es inyectivo.

- **Mitiga** la ambiguedad, no la elimina.

### Traducción única

> $`C`$ ningún sufijo colgante es idéntico a alguna palabra clave del código.

- **Elimina** la ambiguedad.

Comprobación:

1. Conjunto $`L_{0}`$ con todas las palabras clave.
2. Conjunto $`L_{i} \in L_{i+1} \cup SC_{i}`$ donde:
	- $`SC_{i} :`$ sufijos colgantes de todas las palabras de $`L_{i}`$
3. REPETIR (2) HASTA
	- Obtengo sufijo colgante idéntico a alguna palabra en $`L_{0}`$
		- **No es traducción única.**
	- No obtengo nuevos sufijos colgantes.
		- **Es traducción única.**

Ejemplo:

1. L0 = {1, 10, 11, 01}
2. L1 = L0 + {0, 1}
	- (1, 10) → 0
	- (1, 11) → 1 → Halt!
	- ...
- No es traducción única, 1 en L0.

Ejemplo:

1. L0 = {0, 01, 11}
2. L1 = L0 + {1}
	- (0, 01) → 1
	- (0, 11) →
	- (01, 11) →
3. L2 = L1 + {1}
	- (1, 0) →
	- (1, 01) →
	- (1, 11) → 1
4. Halt!
- Es traducción única, no hubo nuevos sufijos colgantes.

### Instantáneo/Prefijo

> $`C`$ ninguna palabra clave es prefijo de otra.

#### Teoremas

> Th. McMillan: si $`C`$ es traducción única, $`K(C) = \sum_{i=1}^{n}D^{-l_i} ≤ 1`$ donde:

- **n:** nº de palabras clave.
- **D:** aridad alfabeto.
- **l:** longitud de palabra.

> Th. Kraft: $`\exists C : Prf(C) ⇔`$ Th. McMillan para C

- **Prf:** es código prefijo.

> Th. Kraft-McMillan: si existe código traducción única → existe uno equivalente instantáneo.

#### Construcción

1. Lista con longitudes que cumple Th. McMillan.
2. Construir árbol.
3. Asignar las palabras solo a los nodos hoja.

Ejemplo:

1. [2, 2, 3, 3, 3, 3]
2. Árbol.
3. Asignaciones.
	1. 2 → 00
	2. 2 → 01
	3. 3 → 100
	4. 3 → 101
	5. 3 → 110
	6. 3 → 111

![Árbol McMillan](img/arbol_mcmillan.png)

### Longitudes

- **Uniforme:** todas las palabras clave tienen misma longitud, necesita $`\log(n)`$ bits mínimo.
- **No uniforme:** palabras clave con varias longitudes, necesita $`H`$ bits mínimo.

# Fuente

> Emite de forma **aleatoria y periódica,** símbolos de un alfabeto.

- Aleatoria porque si no, sería predecible y por tanto innecesario almacenar memoria e.g fichero vs. sucesión de Fibonacci. El fichero no es predecible (almacenar), la sucesión no necesita almacenarse toda, solo los casos base y recursivo (calcular).
- Periódica porque no se puede transmitir todo a la vez.

Una fuente discreta puede modelarse mediante un proceso estocástico (dependiente del tiempo). Es decir, como secuencia de variables aleatorias $`X_{i}`$ donde cada i-ésimo es una letra emitida por la fuente.

**Fuentes sin memoria:** letras independientes.

```math
p_{i}
= P(X_{i} = x_{i})
, \sum_{i = 1}^{n} p_{i} = 1
```

**Fuentes con memoria:** letras dependientes las m-emitidas.

```math
p_{i_{m+1}}
= P(X_{i_{m+1}} = x_{i} | x_{1}, ···, x_{i_{m}})
, \sum_{i = 1}^{n} p_{i_{m+1}} = 1
```

Por ejemplo, en español $`P(e | qu) = 1`$

# Información

La información de una letra es la probabilidad de ocurrencias $`I(x_{i}) = I(p_{i})`$

1. $`I(x_{i}) ≥ 0`$
2. $`I(x_{i}) ≥ I(x_{j}) ⇔ p_{i} ≤ p_{j}`$
3. $`I(x_{i}, x_{j}) = I(x_{i}) + I(x_{j}) ← P(x_{i}, x_{j}) = P(x_{i}) · P(x_{j})`$
4. Función continua, monótona y pequeños cambios en la probabilidad conlleva pequeños cambios en la información.

Solo existe una función con esas condiciones.

```math
I(x_{i}) = I(p_{i}) = - \log_{b}(p_{i}) = \log_{b} \left( \frac{1}{p_{i}} \right)
```

---

Interpretaciones:

1. La no información es 0, no negativo.
2. 00100 vs. 10101. El 1 del primero tiene más información que el del segundo, ya que es más llamativo/destacado/desviado.
3. "Ha nevado en Murcia". En principio no me lo creo, pero si todas las noticias lo dicen → más evidencia → me lo creo. Sin embargo, las evidencias deben ser independientes. Si todas las noticias se basasen en una sola noticia, pierde valor, no gana!
4. xD.
