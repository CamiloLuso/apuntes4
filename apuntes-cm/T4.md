# Compresión estadística

> Usar las probabilidades de los símbolos para codificar.

Tipos:

- Shannon (no óptimo).
- Huffman (óptimo, a veces).
- Aritmético.
- Predictivo.

# Prólogo

Es posible construir **códigos instantáneos** para cualquier distruibución de $`\left\{p \right\}`$:

```math
H(X) ≤ \bar{n} < H(X) + 1
```

- **Óptimo:** $`\bar{n} = H(X) ⇔ ∀x : I(x) \in \mathbb{N}`$ 
- **Compacto:** dada una distribución de $`\left\{p \right\}`$, no existe otro código con $`\bar{n}`$ menor.

**Redefinición**

> **Código "óptimo":** código es prefijo y (o es óptimo o es compacto).

# Codificación Huffman

## Teorema Huffman

> **Th. Huffman:** si un **código es prefijo óptimo,** entonces cumple:

1. $`p_j ≥ p_i → l_j ≤ l_i`$, mayor probabilidad → menor longitud.
2. Las 2 palabras clave asociadas a las 2 letras menos probables tienen misma longitud.
3. Las 2 palabras clave más largas son idénticas excepto último dígito.

> Código prefijo óptimo → cumple 3 condiciones.

**Ejemplo**

| $`x`$ | $`p`$ | $`C_{Huff}(x)`$ |
| :-: | :-: | :- |
| b | 0.4 | 1    |
| a | 0.2 | 01   |
| c | 0.2 | 000  |
| d | 0.1 | 0010 |
| e | 0.1 | 0011 |

**Consecuencias**

- **Reducción de fuente:** (Hojas → Raíz) Construir árbol.
- **Ampliación de código:** (Raíz → Hojas) Etiquetar nodos.

## Método Huffman

1. Ordenar letras por probabilidades decrecientes.
2. **Reducir fuente** agrupando las 2 últimos letras.
3. REPETIR (1, 2) HASTA alfabeto 2-ario.
4. **Ampliar código** anexando 0 y 1 a las 2 letras
5. REPETIR (4) HASTA alfabeto n-ario-original.

**Ejemplo I**

![Código Huffman](img/codigo_huffman_1.png)

**Ejemplo II**

![Código Huffman](img/codigo_huffman_2.png)

**Fíjate que los cuadraditos son los mismos, pero en orden arborescente.**

**Corolario**

- La etiquetación puede ser completamente arbitraria.
	- Podemos obtener $`2^{n-1}`$ árboles diferentes.
- (Huffman) # Códigos > # Árboles.
- Todos son "óptimos" (óptimos o compactos).

## Envío

> Para descodificar, es necesario enviar el código junto con la codificación de la fuente.

No hace falta mandar el árbol completo, hay que enviar una lista con 3-tupla **(letra, palabra, longitud):**

- $`[(a, 01, 3), (b, 1101, 4), ...]`$

En el caso de que el código sea reconstruible de por un **árbol Huffman,** se puede enviar solo **(longitud):**

- $`[l_1, l_2, ...]`$

En tal caso, por el teorema de Kraft-McMillan podemos reconstruir el árbol, pero obtener su código no es trivial (se puede, ya que en los árboles Huffman se pueden permutar los 0 y 1).

## Código Huffman canónico

> **Código Huffman canónico** si el código reconstruido por el árbol cumple el **orden lexicográfico.**

- La ventaja es que no necesita recorrer ningún árbol (acelera descodificación).

# Selección de código Huffman

## Mínima varianza

$`σ_C^2 = \sum p_i · (n_i - \bar{n})^2`$

> Dado dos árboles Huffman con la misma $`\bar{n}`$, pero distintas alturas ($`σ^2`$). Es preferible el de altura ($`σ^2`$) mínima.

- **+altura =** $`+σ^2`$
- **-altura =** $`-σ^2`$

## Código Huffman canónico

> De entre los códigos óptimos, los canónicos comprimen y descomprimen más eficientemente.

**Restricciones**

1. Códigos cortos antes que largos. El primero siempre es 0.
2. Todos los códigos de misma longitud tienen valores consecutivos lexicográficamente.

**Ejemplo**

1. 0
2. 10
3. 110
4. 1110
5. 1111

**Construcción**

1. Imponer restricciones.
2. Obtener código Huffman y, a partir de las longitudes obtener código Huffman canónico.

**Ejemplo:** dado (letra, probabilidad)

- (b, 0.4)
- (a, 0.2)
- (c, 0.2)
- (d, 0.1)
- (e, 0.1)

1. **Construir árbol Huffman**
- b = 1
- a = 01
- c = 000
- d = 0010
- e = 0011

---

2. **Sacar del árbol las longitudes de los códigos**
- [1, 2, 3, 4, 4]

---

3. **Construir código lexicográfico con las longitudes**
- 0
- 10
- 110
- 1110
- 1111

---

4. **Asociar (letra, código)**
- b = 0
- a = 10
- c = 110
- d = 1110
- e = 1111
