# WAVE

> Compuesto por **paquetes** que tienen **cabecera y subpaquetes.**

**WAV** es un caso especial de WAVE **sin compresión.**

# Compresión *Lossless*

Se puede usar compresión general como Huffman o diccionarios pero no consideran las propiedades específicas del audio.

## Codificación predictiva

Asumir que las $`x_i`$ no cambian mucho de una muestra a la siguiente. Por tanto, se puede hacer las **diferencias** para **concentrar la PDF.**

**Nota**

- Las diferencias se hacen entre muestra y predicción.
- Los errores de cuantización se acumulan pero con cuantización unitaria se elimina.

## Codificación transformadas

Te jodes.

# Enmascaramiento

> Un sonido enmascara a otro cuando tapa al otro.

El enmascaramiento se puede interpretar como información redundante y se puede eliminar. Tipos:

- Frecuencial.
- Temporal.
