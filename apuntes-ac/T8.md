# Tipos de errores

- **Error de entrenamiento:** error sobre conjunto entrenamiento total (entrenamiento + validación).
- **Error de validación:** error sobre parejas de conjuntos (entrenamiento, validación)
- **Error de test:** error sobre conjunto de test, estimación del error real.

# ¡ADVERTENCIA! Tener en cuenta

> Toda síntesis del análisis está **sujeta al conjunto de datos.** Depende del dominio.

- No existe un algoritmo de aprendizaje general. Existen algoritmos de aprendizaje específicos. Estos nos dicen lo bien que se ajusta sus sesgo inductivo al conjunto de datos.

**Procedimiento**

```
training, testing <- split(data)
model   <- learn(training)
y'      <- predict(model, testing.x)
summary <- evaluate(y', testing.y)

learn(training): model =
    hyperparameters <- crosvalidate(training)
    final <- train(x, y, METHOD, hyperparameters)
    final

crosvalidate(training): hyperparameters = 
    for resample = "CV|K-CV|Bootstrap|LGOCV"
        for i = 1:nresamples(resample, training)
            subtraining, validation <- split(training)
            models[i]    <- train(subtraining.x, subtraining.y, METHOD, GRID)
            y'           <- predict(model, validation.x)
            summaries[i] <- evaluate(y', validation.y)

    best <- hyperparameters(models, summaries)
    best

METHOD = "MLP"
GRID = {
    size = 100:200:10
    iter = 1:20
}
```

# Experimentos centéficos

Un experimento científico es una prueba tal que:

- Obtenemos salidas en función de entradas.
- Existen variables que pueden afectar las salidas.
	- Algunas controlables y otras no.

Se intenta descubrir:

- Qué variables contribuyen a la salida.
- Qué variables contribuyen con mayor importancia.
- Qué valores opimizan la salida.

> **Objetivo:** eliminar aleatoriedad y obtener conclusiones **estadísticamente significativas.**

## Nivel de factor

> **Nivel de factor:** combinación de valores a probar.

Estrategias

- **Best guess:** combinaciones con cierto sentido.
- **1 factor at 1 time:** fijar todas las variables excepto una que varíe.
- **Factorial:** probar todas las combinaciones de todos los niveles de todas las variables.

## Factor In/Controlable

Controlable:

- Algoritmo.
- Hiperparámetros
- Conjunto de datos.
- Representación de datos.
	- Preprocesado.
	- Normalización.
	- Codificación.

No controlables:

- Ruido.
- Conjunto de entrenamiento si hay sampleo aleatorio.
- Parte aleatoria de algoritmos (Naturaleza propia de los algoritmos).
	- GA.
	- Tempe simulado.

# Propiedades

Formas de controlar cosas.

## Randomización

> Es necesario que el orden de evaluación sea aleatorio para que los resultados sean independientes.

## Replicación

> Dada una **configuración fija de _factores controlables,_** el experimento debe ejecutarse **varias veces** para promediar el efecto de los **factores incontrolables.**

- En AC mediante crosvalidación.
- La variación de salida se compara entre configuraciones para saber si existe una diferencia estadísticamente significativa.

## Bloqueo

> Reducir o eliminar la variabilidad en variables "molestas" controlables.

- e.g, en crosvalidación, se debe usar los mismos conjuntos de entrenamiento y validación entre diferentes algoritmos. Ya que el resampleo puede producir un efecto pero de esa forma se bloquea.

# Experimentar

- **Objetivo:** definir problema y objetivos.
    - ¿Comparar algoritmos? ¿probar algoritmos? ¿alcanzar error aceptable?
- **Variable salida:** decidir métricas.
    - Accuracy, MSE.
	- Función de pérdida (permite combinar diferentes métricas y ponderarlas).
- **Variables entrada y niveles:**
	- ¿Hiperparámetros son variables? ¿o constantes?
	- ¿Algoritmo es una variable? ¿o constante?
- **Metodología:**
	- +repeticiones = +fino.
	- Conjuntos pequeños → alta varianza.
- **Experimento:**
	- Guardar resultados intermedios.
	- Usar semillas para reproducir.
	- Evitar sexgos **subjetivos.**
- **Análisis:**
	- Comprobar que conclusiones no son subjetivas ni aleatorias.
    - Significacia estadística.
- **Conclusiones:**
	- **Pruebas estadísticas** son **evidencia** hasta cierto grado de confianza. No son concluyentes.
	- Si resultado ≠ lo esperado → investigar
- **Recomendaciones:**
	- Experimentos son iterativos.
	- Usar <25% de recursos en primer experimento.
	- Antes de probar algo nuevo, asegurarse de que se sabe todo lo posible sobre los datos (ideas = baratas vs. experimentos = caros).

# Validación cruzada

> Utiliza más eficientemente el conjunto de entrenamiento porque divide en parejas (entrenamiento, validación).

**Métodos**

1. **CV:** X / K partes aleatoriamente, K / 2 partes (X grande).
2. **K-CV:** X / K partes aleatoriamente, K repeticiones (X pequeño).
	- V1 = X1, T1 = X2, X3...
	- V2 = X2, T2 = X1, X3...
	- V3 = X3, T3 = X1, X2...
    1. Partes lo más grande posibles.
    2. Solapamiento entre (entrenamiento, validación) lo mínimo posible.
    3. **Estratificación:** mantener la proporción original de X en las divisiones.
3. **5x2:** X / 2 partes, 5 repeticiones.
	- T1 = X1.1, V1 = X1.2
	- T2 = X1.2, V1 = X1.1
	- T3 = X2.1, V1 = X2.2
	- T4 = X2.2, V1 = X2.1
	- ...
	- T9 = X5.1, V9 = X5.2
	- T10 = X5.2, V10 = X5.1
4. **Bootstrapping:** K-CV con muestreo por reemplazamiento (X muy pequeño).
	- Solapamiento entre (entrenamiento, validación) muy alto.

# Rendimiento

| **Positivo** | **Negativo** | **Total** |
| :-: | :-: | :-: |
| **TP** | FN     | p |
| FP     | **TN** | n |
| p'     | n'     | N |

- **Diagonal:** verdaderos.
- **Triángulo superior:** falsos negativos.
- **Triángulo inferior:** falsos positivos.

**Medidas**

- **Accuracy:** (TP + TN)/N
- **Error:** 1 - Accuracy
- **TP-rate:** TP/p
- **FP-rate:** FP/n
- **Precision:** TP/p'
- **Recall:** TP-rate
- **Sensitivity:** TP-rate
- **Specificity:** 1 - FP-rate.

# ROC

> La **curva ROC** es la relación entre (FP-rate, TP-rate).

Suele haber algún parámetro que, a cambio de incrementar FP-rate, aumente TP-rate.

![ROC](img/roc.png)

- **Línea:** mal.
- **Curva:** bien.

# Intervalo de confianza

> El **intervalo de confianza** es un rango estimado para un parámetro con un grado de confianza.

![Intervalo de confianza](img/confianza.png)

Dada una **media muestral** podemos predecir/estimar que la **media poblacional** está entre un **rango [-Z, Z]** con un **1 - α% de confianza.**

```math
X \sim N(μ, σ^2)
```

```math
Z \sim \sqrt n \frac{\bar{x} - μ}{σ}
```

- Conocemos **varianza poblacional** → distribución normal.
- Conocemos **varianza muestral** → distribución t-Student.

# Hipótesis

1. Hipótesis (=; ≠; ≤ ...)
2. Test.
    - Intervalos de confianza.
3. Aceptar|Rechazar hipótesis.

## Test binomial

> Se ajusta a una **distribución de Bernoulli** tal que 1 si acierto en conjunto de validación y 0 en otro caso.

- $`H_0: p ≤ p_0`$
    - $`p`$ error.

**Aceptar H** si test < 0.95.

> Aplicable cuando tenemos 1 conjunto entrenamiento, 1 conjunto validación.

## Test normal

- $`H_0: p ≤ p_0`$
    - $`p`$ error.

**Aceptar H** si test < Z.

> Aplicable cuando tenemos $`n`$ no es pequeño y $`p`$ no es muy cercano ni a 0 ni a 1.

## Test t, 1 muestra

- $`H_0: p_i ≤ p_0`$
    - $`p`$ error.

**Aceptar H** si test < t.

> Aplicable cuando apliquemos K-CV.

## Test t, 2 muestras

- $`H_0: p_i = 0 : p_i = p_{1,i} - p_{2,i}`$
    - $`p`$ error.

**Aceptar H** si test entre [-t, t]

> Aplicable cuando comparamos dos algoritmos que usaron los mismos conjuntos (entrenamiento, validación).

## Test McNemar

> Se ajusta a una distribución $χ^2$ con un grado de libertad.

Se puede calcular a partir de la tabla:

| **Incorrect** | **Correct** |
| :-: | :-: |
| IB  | I12 |
| I21 | CB  |

- **IB:** incorrect both.
- **I12:** incorrect 1 but not 2.
- **I21:** incorrect 2 but not 1.
- **CB:** correct both.

---

- $`H_0: e_1 = e_2`$
    - $`e`$ error.

**Aceptar H** si test < $`χ^2`$

> Aplicable cuando comparamos dos algoritmos que usaron los mismos conjuntos (entrenamiento, validación).

## p-value

> El **p-value** apoyo a la hipótesis nula.

Se puede calcular directamente el p-value y compararlo con el α.

- -p-value = rechaza hipótesis nula.
- +p-value = acepta hipótesis nula.

Por lo general, en la hipótesis nula pondremos lo contrario a demostrar.

- **Error 1:** p-value no es probabilidad sobre la hipótesis nula. Es probabilidad sobre compatibilidad sobre la hipótesis nula.
- **Error 2:** p-value < 0.05 = hipótesis nula falsa y p-value > 0.05 verdadera. No es cierto, son probabilidades, todavía se podría falsificar.
- **Error 3:** -p-value = +fiable el p-value. No es cierto, es probabilidad de compatibilidad.

# xD

- Intervalo de confianza estima parámetro. Hipótesis comprueba si el parámetro es =, ≠, ≤ ...
