# Aprendizaje no supervisado

> $`h(X)`$.

Buscar relaciones en $`X`$. Fases:

1. X / k conjuntos exhaustivos y exclusivos.
2. Clasificar según las etiquetas de las particiones.

# Clustering

> División tal que $`\sum σ^2_i`$ mínima, nº clústeres mínimo.

- **Extremos:**
    - 1 clúster = 1 punto → $`σ^2 = 0`$, |clústeres| = máximo.
    - 1 clúster = todos → $`σ^2`$ máxima, |clústeres| = mínimo.
- **Compromiso:**
    - $`σ^2`$ mínima, |clústeres| = mínimo.
- **Unir** → +$`σ^2`$, -|clústeres|
- **Dividir** → -$`σ^2`$, +|clústeres|

> **Normalizar** es importante para que todas las variables tengan la misma importancia.

# Clústering distancias euclídeas

> Similitud entre puntos del espacio.

```
centroids <- random(length = K, dim = dim(X))

m = 0
for x_i in X
    centroid <- nearest(centroids, x_i)
    centroid <- mutate(centroid, x_i, alpha(m))
    m <- m + 1

mutate(p, x, alpha): centroid = (1 - alpha)*p + alpha*x

alpha(m): alpha = 1/(1 + m)
```

- **Nota 1:** los prototipos o centroides no tienen porqué ser un subconjunto de X
- **Nota 2:** los prototipos tienen longitud = k y dimensión = X.
- **Nota 3:** prototipo = ejemplo necesariamente pertenece a X, centroide = ejemplo no necesariamente pertenece a X.

**Métricas:** media y varianza.

```math
μ = \frac{1}{k} \sum_{i = 1}^{k} x_i
```

```math
σ^2 = \frac{1}{k} \sum_{i = 1}^{k} (x_i - μ)^2
```

Por último, se hacen particiones de Voronoi.

# Clústering k-medias

> **Cuantización:** mapear un espacio continuo a discreto.

```
m <- random(length = K)

while not converge(m)
    for x_i in X
        b <- b(x_i, m)
    for m_i in m
        m_i <- wmean(b, X)

b(x, m): b =
    if abs(x - m) == min(abs(x - m))
    then 1
    else 0
```

El objetivo es buscar los valores `m_i` que minimicen el error. El procedimiento es básicamente calcular b. ajustar m. La eficacia depende de la inicialización aleatoria de m.

- Muestrear k instancias de X.
- Media de X + random offset.
- PCA, dividir en k grupos, media de los k grupos.

# Clústering distribuciones

> El **sesgo** es asumir que los clústeres siguen alguna distribución de probabilidad. Si no la siguen, las salidas son pobres.

# Clústering Bayes

1. C = X / k
2. $`P(C_i | x_i) = P(x_i | C_i)P(C_i)`$ maximizar..
3. $`S(X, C) = P(C | X_i)·P(C | X_{i-1})`$ es la similitud con el cluster C.

```
C = ()
Cmax = {}
for x in X
    for c in C
        if S(x, C) > DELTA
        then Cmax <- Cmax + x
        else C    <- C + x

        C <- joins(C)

joins(C): C =
    for ci in C
        for cj in C
            if (mean(ci) - mean(cj))^2 < EPSILON
            then Cnew <- ci + cj
                 C <- C - ci - cj + Cnew
            else ;
    C
```

# EM

> Cuando existen variables ocultas pero conocemos la distribución de probabilidad. Estimar k distribuciones.

1. Construir $`X`$ mediante las $`k`$ distribuciones.
2. Toda distribución $`k`$ construimos $`x_i`$ según su PDF.

**Ejemplo:** PDF uniforme, conocemos $`σ^2`$ y misma para todas.

El objetivo es construir una hipótesis $`h = (μ_1, ..., μ_k)`$ (medias de las k distribuciones). Según un enfoque bayesiano el objetivo es maximizar $`P(X | h)`$.

1. Toda instancia $`(x_i, z_{i,j}, ..., x_{i,k})`$.
2. Probar toda instancia.
    - $`x_i \in X`$
    - $`z_{i,j}`$: estimación basada en la $`h`$ anterior.
        - 1 si fue construido por la distribución $`j`$.
        - 0 si no.
3. Calculamos $`h`$ según las instancias.
4. Repetir si no es suficientemente estable.

**Conclusión**

1. Cada iteración mejora $`P(X|h)`$
2. Es posible caer en máximos locales.
3. Se puede generalizar para un conjunto de parámetros $`θ`$

# Clústering densidades

> Delimitan zonas del espacio donde haya alta de frecuencias de ejemplos.

- Según el algoritmo excluyen o no a los *outliers.*

# Clústering difuso

> Todos los puntos pertenecen a todos los clústeres pero con cierto gradode pertenencia.

- Conjuntos difusos con colas infinitas → se cubre todo el espacio.
- Ejemplos que no tengan un mínimo de pertenencia se pueden considerar dudosos.

# Clústering jerárquico

Solo es necesario una métrica de distancia. El parámetro k no tiene sentido pues se crea una jerarquía.

```math
d(v,u,p) = \left[ \sum_{i=1}^{n} (v_i - u_i)^p \right]^{\frac{1}{p}}
```

- Minkowski. Euclídea $`p = 2`$. Manhattan $`p = 1`$.

```math
d(v,u) = \sum_{i=1}^{n} |v_i - u_i|
```

- Manhattan es +eficiente computacionalmente.

---

- **Enlace simple:** distancia entre dos grupos = distancia mínima entre dos puntos de diferente grupo.
- **Enlace completo:** distancia entre dos grupos = distancia máxima entre dos puntos de diferente grupo.
- **Enlace promedio.**
- **Enlace centroides.**

## Aglomerar

- **Inicio:** N = |X| clústeres.
- **Fin:** 1 clúster.

## Dividir

- **Inicio:** 1 clúster.
- **Fin:** N = |X| clústeres.

# ¿Cuántos clústeres hay en los datos?

> ¿Cómo encontrar el mejor k?

- Lo establece el problema. k = 256 colores.
- Experto.
- Distancia máxima en un mismo clúster.
- Curva de alguna métrica (después de cierto k el error no varía significativamente).

# Ensemble de clústeres

> Combinar varios algoritmos o mismo algoritmo distintos hiperparámetros.
