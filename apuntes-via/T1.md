# Cámara

> Una **cámara** es un medidor de ángulos, como un tranportador de ángulos pero no en **escala curva** sino **recta.** Por tanto, puede convertir ángulos en distancias.

- Distancia (1D).
- Tamaño (ND).
- Píxel.
- FOV.
    - Horizontal.
    - Vertical.
- Ratio.
    - Resolución.
        - Horizontal.
        - Vertical.

# Modelo Pinhole

![](img/pinhole.png)

```math
x = f\frac{X}{Z}
```

```math
y = f\frac{Y}{Z}
```

```math
\tan (α) = \frac{x}{f}
```

```math
\tan (α) = \frac{y}{f}
```

- $`f`$ distancia focal (píxel (depende de la resolución)).
- $`α`$ FOV/2 (grado).

# Modelo

> (Imagen, Máscara) → Etiqueta.

- **Máscara:** binaria o real.
- **Etiqueta:** entera.

# Modelos

- Media.
- Elipsoide.
- Intervalos.
- **Reproyección de probabilidad.**
