# λ-Cálculo como lenguaje de programación

- `let I = E1 in E2`
- `E2 where I = E1`

> $`(λ I.E_{2}) E_{1}`$

# Bool

## Constantes

- $`true = (λxy.x)`$
- $`false = (λxy.y)`$

## Operadores

- $`cond = (λpqr.pqr)`$

- $`and = (λxy.xy\ false)`$

- $`or = (λxy. x\ true\ y)`$

- $`not = (λx.x\ false\ true)`$

|  x  |  y  |     ^     |     v     |
| :-: | :-: | :-------: | :-------: |
|  T  |  T  | T **T** F | T **T** T |
|  T  |  F  | T **F** F | T **T** F |
|  F  |  T  | F T **F** | F T **T** |
|  F  |  F  | F F **F** | F T **F** |

# Tupla

## Constructor

- $`(E_{1}, E_{2}) = (λf.f E_{1} E_{2})`$

## Destructor

- $`fst = (λp.p\ true)`$
- $`snd = (λp.p\ false)`$

Proyecciones:

- $`(p)_{1} = fst\ p`$
- $`(p)_{n} = snd^{n-1}\ p`$
- $`(p)_{i} = fst(snd^{i-1}\ p) : 1 < i < n`$

## Genérica

- $`(T)^{k} U = (T ··· (T U) ···)`$
	- $`(T)^{0} U = U`$

> $`(E_{1}, ··· E_{n}) = (E_{1}, (··· (E_{n-1}, E_{n})···))`$

## Curry

> Función $`f`$ n-aria $`n \ge 2`$

- $`curry_{n}\ f = (λx_{1} ··· x_{n}.f(λx_{1}, ···, x_{n}))`$
	- Pasar $`f`$ con entrada tupla a curry.
- $`uncurry_{n}\ g = (λp.g(p)_{1} ··· (p)_{n})`$
	- Pasar $`f`$ curry a entrada tupla.

> $`curry(uncurry\ f) = f`$

Identificar:

- **Currificada:** $`f x y`$
- **Uncurrificada:** $`f (x, y)`$

### Curioso

$`and\ true = (λxy.xy\ false)\ true → (λy.true\ y\ false)`$ es la función identidad $`I = (λx.x)`$.

# Natural

> $`n = (λfx.f^{n}x)`$

- $`0 = (λfx.x)`$
- $`1 = (λfx.fx)`$
- $`2 = (λfx.f(fx))`$
- $`3 = (λfx.f(f(fx)))`$
- ...

# Operaciones

> $`SUCC = (λnfx.f(nfx))`$

> $`+ = (λmnfx.(mf)(nfx))`$

> $`× = (λmnfx.m(nf)x)`$

> $`∧ = (λmnfx.nmfx)`$

> $`ISCERO = (λn.n(λx.false)true)`$

La función predecesor para que funcione bien con 0, se basa en otra predecesor auxiliar. De este modo, $`PRED(0) = 0`$

# Recursión

> Se podría ver como una función que se tiene a sí misma como argumento. Sin embargo, las funciones lambda no tienen nombre, por lo que debe haber un método que permita a una función llamarse a sí misma sin utilizar su nombre.

Ejemplo, factorial no válido:

$`(λfx. ISCERO\ x → 1\ ;\ x × f (PRED x))`$

- No es válida, pues $`f`$, al estar ligada, podría ser cualquier otro término o función. Por tanto, es necesario:

$`fact = (λx. ISCERO\ x → 1\ ;\ x × fact (PRED x))`$

Equivalente:

$`fact = (λfx. ISCERO\ x → 1\ ;\ x × f (PRED x)) fact`$

> Esto significa que $`fact`$ es un **punto fijo** de la función $`fact`$. Es decir, que cumple $`f(x) = x`$

# Operador Y

> $`Y = (λG. (λg.G(gg))(λg.G(gg)))`$

De forma que:

$`Y F = F (Y F)`$

---

Y es el combinador de punto fijo, combinador paradógico u operador de búsqueda del punto fijo (si existe). Por tanto, $`Y F`$ es un punto fijo de $`F`$

# Let

> Es como **declarar** el valor de los párametros **en** una función.

$`\mathrm{let}\ x_1 = s_1\ \mathrm{and}\ ··· x_n = s_n\ \mathrm{in}\ t`$

$`(λx_1 ··· x_n.t)s_1 ··· s_n`$

Let se interpreta como no recursivo. Cuando hace una definición recursiva:

$`\mathrm{let\ rec}\ f = Y(λf.x)\ \mathrm{in}\ t`$

