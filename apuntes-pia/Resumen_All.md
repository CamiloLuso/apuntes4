# Cálculo Lambda

## Sintaxis

```math
$$T := v | c | T T | \lambda v . T
```

1. Término (Expresión).
2. Variable.
3. Constante.
4. Aplicación (Combinación).
5. Abstracción.

### Asociatividad

- **Izquierda:**
	- Aplicación: $`f x y = ((f x) y)`$
- **Derecha:**
	- Abstracción: $`\lambda x . xy = (\lambda x . (xy))`$
	- Curry: $`\mathbb{R} \rightarrow \mathbb{R} \rightarrow \mathbb{R} =
                  \mathbb{R} \rightarrow (\mathbb{R} \rightarrow \mathbb{R})`$

### _Free vs. Bound variable_

Para toda variable $`x`$ y términos $`s,t`$ es *free:*

1. $`F(x) = \{x\}`$
2. $`F(st) = F(s) \cup F(t)`$
3. $`F(\lambda x . s) = F(s) - \{x\}`$

Para toda variable $`x`$ y términos $`s,t`$ es *bound:*

1. $`B(x) = \varnothing`$
2. $`B(st) = B(s) \cup B(t)`$
3. $`B(\lambda x . s) = B(s) \cup \{x\}`$

Si $`F(s) = \varnothing`$, entonces $`s`$ es un combinador.

## Sustitución

Sustituir $`s[y/x]`$ las $`x`$ por las $`y`$.

## Conversiones

- **α-conversión:** renombramiento.
	- $`\lambda x . s \rightarrow \lambda y . s[y/x] : y \not \in F(s)`$
	- Ejemplo: $`\lambda x . xy \rightarrow \lambda z . zy`$
	- Contraejemplo: $`\lambda x . xy \not\rightarrow \lambda y . yy`$
- **β-conversión:** aplicación.
	- $`(\lambda x . s) a \Rightarrow s[a/x]`$
	- $`(\lambda x y . x + y) 1 2 \Rightarrow
           (\lambda y . 1 + y) 2 \Rightarrow
           1 + 2`$
- **η-conversión:** extensión.
	- $`\lambda x . sx \rightarrow s : x \not \in F(s)`$
	- Ejemplo: $`\lambda x . yx \rightarrow y`$
	- Contraejemplo: $`\lambda x . xx \not\rightarrow x`$

La función $`F(s)`$ devuelve el conjunto de *free variables* dentro de $`s`$.

### Igualdad

$$
\frac{
s \underset{\alpha}{\rightarrow} t \vee
s \underset{\beta}{\rightarrow} t \vee
s \underset{\eta}{\rightarrow} t
}
{s = t}
$$

$$
\frac{}{t = t}
$$

$$
\frac{s = t}{t = s}
$$

$$
\frac{s = t \wedge t = u}{s = u}
$$

$$
\frac{s = t}{su = tu}
$$

$$
\frac{s = t}{us = ut}
$$

$$
\frac{s = t}{\lambda x . s = \lambda x . t}
$$
