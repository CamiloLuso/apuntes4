# Lambda cálculo

## Sintaxis

**Alfabeto**

- Variables: $`x_{1}, x_{2}, ...`$
- Símbolos impropios: $`λ, ., (, )`$
- Igualdad: $`=`$
- Reducción: $`→`$

**Términos**

```math
T ::= v | (T T) | (λ x. T)
```

**Fórmulas**

Si $`M`$ y $`N`$ son términos:

- $`M = N`$
- $`M → N`$

son fórmulas.

## Pureza

- **Puro:**
	- No permite constantes.
	- Permite combinadores.
- **No puro:**
	- Permite constantes y funciones predefinidas (0, 1, succ(), sum()).

## Notación convencional

### Aplicación asociativa izquierda

$`E_{1} E_{2} \cdots E_{n} = ((E_{1} E_{2}) \cdots E_{n})`$

### Abstracción extiende derecha

$`λ x. E_{1} E_{2} \cdots E_{n} = (λ x. (E_{1} E_{2} \cdots E_{n})) `$

### Abstracción abreviación

$`(λ xyz.E) = (λ x . (λ y . (λ z . E)))`$

## _Free_ vs. _Bound_

Para toda variable $`x`$ y términos $`M, N`$ es *free:*

1. $`F(x) = \{x\}`$
2. $`F(M N) = F(M) \cup F(N)`$
3. $`F(λ x . M) = F(M) - \{x\}`$

Para toda variable $`x`$ y términos $`M, N`$ es *bound:*

1. $`B(x) = \varnothing`$
2. $`B(M N) = B(M) \cup B(N)`$
3. $`B(λ x . M) = B(M) \cup \{x\}`$

**Resumen**

Es ligada si se encuentra dentro del ámbito de una abstracción $`(λ x . E)`$. Los ámbitos son locales; se atribuye al ámbito más interno.

# Sustitución

> Sustituir término $`N`$ en **variables libres.**

```math
\begin{aligned}
x[N/x] &= N\\ 
y[N/x] &= y , y \ne x\\ 
(M K)[N/x] &= (M[N/x])(K[N/x])\\ 
(λ x.M)[N/x] &= (λ x.M)\\ 
(λ y.M)[N/x] &= (λ y.M[N/x]) , y \ne x \wedge y \notin FV(N)
\end{aligned}
```

1. Variable (ocurrencia).
2. Variable (no ocurrencia).
3. Aplicación.
4. Abstracción (en ámbito, nada!).
5. Abstracción (no en ámbito, y no es capturada!)

## Captura de variable

> Al sustituir, si $`N`$ tiene alguna variable libre de la abstracción.

El caso $`(λ y.M)[N/x]`$

1. (Comprobación cruzada) $`y \notin FV(N)`$ o $`x \notin FV(M)`$, seguir normalmente.
2. (Captura) otro caso (Renombramiento).

(Renombramiento)

1. Elegir $`z \notin FV(M) \cup FV(N)`$
2. $`(λ z.M[z/y][N/x])`$

> **Resumen:** captura si ambas variables son libres (cruzadamente).

Ejemplo:

1. $`(λx.xy)[x/y] → (λx.x"x")`$ ¡Captura!
2. $`(λa.xy)[a/x][x/y]`$
3. $`(λa.ay)[x/y]`$
4. $`(λa.ax)`$

Ejemplo encubierto:

1. $`(λ y.x(λ x.x)z)[(λ w.wy)/z] → (λ y.x(λ x.x)(λ w.w"y"))`$ ¡Captura!
2. $`(λ a.x(λ x.x)z)[a/y][(λ w.wy)/z]`$
3. $`(λ a.x(λ x.x)z)[(λ w.wy)/z]`$
4. $`(λ a.x(λ x.x)(λ w.wy))`$

**Ejemplo:**

1. $`x(λyz.x)[z/x] → z(λyz.'z')`$ ¡Captura!
2. $`x(λya.x)[z/x]`$
3. $`z(λya.z)`$

> **Nota:** viene de la β-redex $`(λx.x(λyz.x))z`$

# Reducción

## Reglas

### α-conversión (Renombramiento)

> $`(λ x . M) → (λ y . M[y/x]) : y \notin FV(M)`$

- Ejemplo: $`(λ x . xy) → (λ z . zy)`$
- Contraejemplo: $`(λ x . xy) \not→ (λ y . yy)`$

¡Posible la captura de variables!

### β-conversión (Evaluación)

> $`(λ x . M) N → s[N/x]`$

1. Eliminar $`λ x \implies`$ esas $`x`$ ahora serán libres.
2. Sustituir $`x`$
3. ¿Captura de variables?
	- Sí, renombrar, GOTO 2.
	- No, FIN.

- Ejemplo: $`(λ x y . x + y) 1 2 → (λ y . 1 + y) 2 → 1 + 2`$
- Contraejemplo: $`(λx.(λy.xy))y → (λy.xy)[y/x] → (λy.yy)`$ ¡Captura!
	1. $`(λy.xy)[a/y][y/x]`$
	2. $`(λa.xa)[y/x]`$
	3. $`(λa.ya)`$

¡Posible la captura de variables!

### η-conversión (Extensión)

> $`(λ x . Mx) → M : x \notin FV(M)`$

- Ejemplo: $`(λx.(λy.y)x) → (λy.y)`$
- Contraejemplo: $`(λx.(λy.xy)x) \not → (λy.xy)`$
	- Mal, no se puede arreglar (no usar). Se debe mantener la ligadura de la $`x`$

¡Posible captura de variables!

## Propiedades

```math
\frac{
S \underset{\alpha}{→} T \vee
S \underset{\beta}{→} T \vee
S \underset{\eta}{→} T
}
{S → T}
```

```math
\frac{}{T → T}
```

```math
\frac{S → T \wedge T → U}{S → U}
```

```math
\frac{S → T}{SU → TU}
```

```math
\frac{S → T}{US → UT}
```

```math
\frac{S → T}{λ x . S → λ x . T}
```

# Igualdad/Identidad

> $`M = N`$ si existe una secuencia infinita de $`\alpha , \beta , \eta`$ reducciones que conecten ambos términos.

$`(λ x.x) = (λ y.y)`$ pero $`(λ x.x) \not\equiv (λ y.y)`$

La relacion de igualdad es **extensional;** si dos funciones $`f, g`$ dan iguales resultados para todos los argumentos, entonces son iguales.

> La $`\eta`$-reducción es válida $`\Leftrightarrow`$ principio de extensionalidad es válido.

## Propiedades

```math
\frac{
S \underset{\alpha}{→} T \vee
S \underset{\beta}{→} T \vee
S \underset{\eta}{→} T
}
{S = T}
```

```math
\frac{}{T = T}
```

```math
\frac{S = T}{T = S}
```

```math
\frac{S = T \wedge T = U}{S = U}
```

```math
\frac{S = T}{SU = TU}
```

```math
\frac{S = T}{US = UT}
```

```math
\frac{S = T}{λ x . S = λ x . T}
```

# Forma normal

## Redex

> **Redex:** un término $`M = (λx.U)`$ y $`N = U[T/x]`$ tal que $`M → N`$

- **Radical:** M.
- **Contracto:** N.

## Normalizable

> **Normalizable:** si existe un elemento normal $`N`$ tal que $`M → N`$

## Normal

> **Normal** si el término no contiene ningún redex.

1. $`N`$ es una variable.
2. $`N`$ es una abstracción $`(λx.T)`$ donde $`T`$ no tiene ningún redex.
3. $`N`$ es una aplicación $`(E_{1} E_{2})`$ donde no tienen ningún redex Y $`E_{1}`$ no es una abstracción $`(λx.T)`$.

# Preguntas estratégicas

**(1) ¿Cualquier expresion lambda puede ser reducida a forma normal?**

No. Por ejemplo, $`((λx.xx)(λx.xx))`$ no puede llegar a la forma normal.

**(2) ¿Hay mas de una manera de reducir una expresion lambda?**

Sí. Por ejemplo, $`(λy.k)((λx.xx)(λx.xx))`$

**(3) Si hay mas de una estrategia de reduccion que conduzca a una forma normal, ¿todas las formas normales obtenidas son iguales?**

Sí, 1er teorema de Church-Rosser.

**(4) Si un λ-termino puede ser reducido a forma normal, ¿existe una estrategia de reduccion que garantize siempre obtenerla?**

Sí, 2o teorema Church-Rosser.

# Evaluación

- **Orden normal:** reducir los $`\beta`$-redex **más exterior más izquierdo** (*leftmost outermost*).
- **Orden aplicativo:** reducir los $`\beta`$-redex **más interior más izquierdo** (*lefmost innermost*)

$`(λy.k)((λx.xx)(λx.xx))`$

- Orden normal; llamada por nombre (declarativo) termina.
- Orden aplicativo; llamada por nombre (imperativo) bucle infinito.

# Teoremas Church-Rosser

> Th1. Si $`S → T_{1} \wedge S → T_{2}`$, entonces $`\exists U : T_{1} → U \wedge T_{2} → U`$

Da igual el tipo de reducción usada, si los términos son iguales, la forma normal es única. Consecuencia es que *la lambda igualdad no es trivial, ya que dos términos que estén en formal normal y que son $`\alpha`$ no tienen por qué ser idénticos.*

> Th2. Si un λ-término puede ser reducido a formal normal, se puede llegar a ella reduciendo sistemáticamente según el **orden de reducción normal.**

# Combinadores

> **Combinador:** término sin variables libres. Su semántica es fija, independientemente de los valores de las variables.

- $`I = (λx.x)`$ - Identidad.
- $`K = (λxy.x)`$ - Constante.
- $`S = (λfgx.(fx)(gx))`$ - Comparte/Aplicación.
